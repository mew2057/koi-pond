﻿using UnityEngine;
using System.Collections;

public enum ButtonOption{
	Play, Exit, Settings, Main
}

public class Button  : MonoBehaviour {
	public ButtonOption ButtonType;
	void ExitGame(){
		Application.Quit ();
	}

	void PlayGame(){
		GameManager.Instance.LoadLevel(1);
	}

	void OpenSettings(){
		MenuController.Instance.TogglePosition();
	}
	void CloseSettings(){
		MenuController.Instance.TogglePosition();
	}

	void PerformOperation(){
		switch(ButtonType){
			case ButtonOption.Exit:
				ExitGame();
				break;
			case ButtonOption.Play:
				PlayGame();
				break;
		case ButtonOption.Settings:			
				OpenSettings();
				break;
		case ButtonOption.Main:
				CloseSettings();
				break;
		}
	}

	void OnMouseDown(){
		PerformOperation();
	}

	void OnMouseOver(){
		renderer.material.color=Color.red;

		/*if(Input.GetMouseButtonUp(0))
			PerformOperation();*/
	}

	void OnMouseExit(){
		renderer.material.color = Color.white;
	}
	
}
