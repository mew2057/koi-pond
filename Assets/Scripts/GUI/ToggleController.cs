﻿using UnityEngine;
using System.Collections;

public enum Toggleable{
	Music,SFX,LongPress
}

public class ToggleController : MonoBehaviour {

	public Toggleable ToggleType;
	public Toggle     ToggleItem;

	// Use this for initialization

	void Start () {
		switch(ToggleType){
			case Toggleable.Music:
				ToggleItem.Toggled = GameManager.Instance.IsMusicPlaying();
				//ToggleItem.SetToggle();
			break;
			case Toggleable.SFX:
				ToggleItem.Toggled = GameManager.Instance.IsSFXEnabled();
				
				//ToggleItem.SetToggle();
				
			break;
			case Toggleable.LongPress:
			ToggleItem.Toggled =  GameManager.Instance.LongPressMenu;
			break;
		}


	}
	
	// Update is called once per frame
	void Update () {
		if(ToggleItem.ToggledFlag)
		{
			switch(ToggleType){
			case Toggleable.Music:
				GameManager.Instance.SetMusicPlaying(ToggleItem.Toggled);
				break;
			case Toggleable.SFX:
				GameManager.Instance.SetSFX(ToggleItem.Toggled);
				break;
			case Toggleable.LongPress:
				GameManager.Instance.LongPressMenu = ToggleItem.Toggled;
				break;
			}
			ToggleItem.ToggledFlag = false;

		}
	}
}
