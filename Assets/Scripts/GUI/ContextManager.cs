﻿using UnityEngine;
using System.Collections;

public enum ContextState{
	Active, Activating, Disabled
}

public class ContextManager : MonoBehaviour {
	public static ContextManager Instance;

	public GameObject ContextMenuObj;
	public float TimeToContext = .5f;
	public float DefaultExitRange = .5f;
	public float ActiveExitRange = 2.0f;
	public Texture2D[] Cursors;

	private float _ExitRange;
	private float _TimeStill = 0.0f;
	private ContextState _MenuState = ContextState.Disabled;

	void Awake(){
		ContextManager.Instance = this;
		_ExitRange = DefaultExitRange;
		_TimeStill = 0.0f;
		_MenuState = ContextState.Disabled;

		SetEquipedCursor(CharacterTool.Skimmer);
	}

	void Update(){
		float mouseDistance = Vector3.Distance(transform.position, GameManager.Instance.CurrentMousePosition);

		if(mouseDistance > _ExitRange){
			transform.position = GameManager.Instance.CurrentMousePosition;
			_TimeStill = 0;

			if(_MenuState == ContextState.Active)
				MenuClear();
			else
				_MenuState = ContextState.Disabled;
		}
		else {
			MouseInRange();
		}
	}

	void MouseInRange(){
		if(GameManager.Instance.LongPressMenu){
			if(Input.GetMouseButton(0) && _MenuState == ContextState.Disabled)
				_MenuState = ContextState.Activating;

			else if(!Input.GetMouseButton(0)  && _MenuState == ContextState.Activating)
				_MenuState = ContextState.Disabled;

			if(_MenuState == ContextState.Activating)
				_TimeStill += Time.deltaTime;
			else
				_TimeStill = 0;

			if(_TimeStill >= TimeToContext )
				ShowContextMenu();
		}
	}

	public void ShowContextMenu(){
		_MenuState = ContextState.Active;
		_TimeStill = 0;
		_ExitRange = ActiveExitRange;

		ContextMenuObj.SetActive(true);
		ContextMenuObj.transform.position = transform.position = GameManager.Instance.CurrentMousePosition;
		 
	}

	public void MenuSelect(CharacterTool selectedTool){
		SetEquipedCursor(selectedTool);
		GameManager.Instance.SetEquippedTool(selectedTool);
		MenuClear();
	}

	void SetEquipedCursor(CharacterTool selectedTool){		
		switch (selectedTool){
		case CharacterTool.Skimmer:
			Cursor.SetCursor(Cursors[0],  new Vector3(16f,16f,0f), CursorMode.Auto);
			break;
		case CharacterTool.Food:
			Cursor.SetCursor(Cursors[1], new Vector3(32f,32f,0f), CursorMode.Auto);		
			break;
		case CharacterTool.WateringCan:
			Cursor.SetCursor(Cursors[2], new Vector3(32f,32f,0f),CursorMode.Auto);	
			break;
		
		}
	}

	void MenuClear(){
		_MenuState = ContextState.Disabled;
		_TimeStill = 0;
		_ExitRange = DefaultExitRange;

		ContextMenuObj.SetActive(false);	
	}

}
