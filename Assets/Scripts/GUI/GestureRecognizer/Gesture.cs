using UnityEngine;
using System.Collections;

public enum GestureOptions{
	Save,Cancel
}

public class Gesture : MonoBehaviour
{
	public static Gesture Instance;
    static GameObject gestureDrawing;
    GestureTemplates m_Templates;

	public GameObject GestureButtonManager;
    public ArrayList pointArr;
    static int mouseDown;
	public bool RecordPattern;

    // runs when game starts - main function
    void Start ()
    {
		if(Instance == null){
			DontDestroyOnLoad(this.gameObject);
			Instance = this;
		}

        m_Templates = new GestureTemplates();
		pointArr = new ArrayList();
    	
	    gestureDrawing = GameObject.Find("gesture");
    }


    IEnumerator worldToScreenCoordinates ()
    {
	    // fix world coordinate to the viewport coordinate
	    Vector3 screenSpace = Camera.main.WorldToScreenPoint(
			gestureDrawing.transform.position);
    	
	    while (Input.GetMouseButton(1))
	    {
		    Vector3 curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
		    Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace); 
		    gestureDrawing.transform.position = curPosition;
		    yield return 0;
	    }
    }

    void Update()
    {
	    if (Input.GetMouseButtonDown(0))
        {
		    mouseDown = 1;
	    }
    	
	    if (mouseDown == 1)
        {
		    Vector2 p = new Vector2(Input.mousePosition.x , Input.mousePosition.y);
		    pointArr.Add(p);
		    StartCoroutine(worldToScreenCoordinates());
	    }


	    if (Input.GetMouseButtonUp(0))
        {
			if (RecordPattern)
			{
			    // if CTRL is held down, the script will record a gesture. 
			    mouseDown = 0;
			    GestureRecognizer.recordTemplate(pointArr);    		
				RecordPattern = false;
		    }
            else
            {
			    mouseDown = 0;

			    // start recognizing! 
			    GestureRecognizer.startRecognizer(pointArr);

			    pointArr.Clear();
    		
		    }
    	}

		if(GestureRecognizer.recordDone == 1)
		{ 
			GestureButtonManager.SetActive(true);
		} 
	} 


	public void DoMyWindow (GestureOptions selectedButton){
		switch(selectedButton){
		case GestureOptions.Save:
			ArrayList temp = new ArrayList();
			ArrayList a = (ArrayList)GestureTemplates.Templates[GestureTemplates.Templates.Count - 1];
			
			for (int i = 0; i < GestureRecognizer.newTemplateArr.Count; i++)
				temp.Add(GestureRecognizer.newTemplateArr[i]);

			print (temp.Count);
			if(temp.Count <= 2){
				GestureButtonManager.SetActive(false);
				GestureRecognizer.recordDone = 0;
				return;
			}

			AddGesture(GameManager.Instance.GestureName, temp);
			
			GestureRecognizer.recordDone = 0;
			GestureRecognizer.newTemplateArr.Clear();
			break;
		case GestureOptions.Cancel:
			GestureRecognizer.recordDone = 0;
			break;
		}
		GestureButtonManager.SetActive(false);
    }

	void AddGesture(string gestureName, ArrayList template){		
		GestureTemplates.Templates.Add(template);
		GestureTemplates.TemplateNames.Add(gestureName);
	}
}
