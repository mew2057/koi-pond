using UnityEngine;
using System.Collections;

// all known gesture templates
public class GestureTemplates
{
    public static ArrayList Templates;
    public static ArrayList TemplateNames;

    public GestureTemplates ()
    {
     
        ArrayList circle = new ArrayList(new Vector2[] { new Vector2(-247, 0), new Vector2(-246, 26), new Vector2(-238, 51), new Vector2(-224, 73), new Vector2(-208, 93), new Vector2(-190, 111), new Vector2(-172, 129), new Vector2(-154, 146), new Vector2(-136, 163), new Vector2(-115, 177), new Vector2(-94, 189), new Vector2(-73, 201), new Vector2(-51, 211), new Vector2(-29, 222), new Vector2(-7, 231), new Vector2(15, 240), new Vector2(39, 242), new Vector2(62, 239), new Vector2(85, 232), new Vector2(109, 227), new Vector2(130, 215), new Vector2(149, 200), new Vector2(168, 185), new Vector2(187, 169), new Vector2(203, 148), new Vector2(218, 128), new Vector2(232, 106), new Vector2(245, 83), new Vector2(252, 58), new Vector2(253, 31), new Vector2(253, 4), new Vector2(250, -23), new Vector2(244, -49), new Vector2(234, -74), new Vector2(222, -97), new Vector2(209, -120), new Vector2(194, -140), new Vector2(178, -160), new Vector2(164, -182), new Vector2(146, -199), new Vector2(126, -212), new Vector2(104, -222), new Vector2(82, -232), new Vector2(59, -240), new Vector2(36, -246), new Vector2(12, -250), new Vector2(-11, -255), new Vector2(-34, -258), new Vector2(-57, -250), new Vector2(-78, -237), new Vector2(-97, -221), new Vector2(-112, -201), new Vector2(-128, -181), new Vector2(-146, -163), new Vector2(-161, -143), new Vector2(-175, -120), new Vector2(-187, -97), new Vector2(-195, -72), new Vector2(-204, -47), new Vector2(-207, -20), new Vector2(-210, 7), new Vector2(-217, 32), new Vector2(-225, 58), new Vector2(-232, 83) });

		ArrayList circle2 = new ArrayList(new Vector2[] {new Vector2(-268,-0),new Vector2(-260,-24),new Vector2(-250,-49),new Vector2(-237,-71),new Vector2(-223,-92),new Vector2(-209,-113),new Vector2(-195,-135),new Vector2(-181,-155),new Vector2(-165,-175),new Vector2(-147,-191),new Vector2(-128,-204),new Vector2(-107,-213),new Vector2(-86,-224),new Vector2(-66,-235),new Vector2(-45,-243),new Vector2(-23,-249),new Vector2(-1,-250),new Vector2(21,-249),new Vector2(43,-248),new Vector2(65,-247),new Vector2(87,-241),new Vector2(108,-233),new Vector2(127,-220),new Vector2(145,-203),new Vector2(161,-185),new Vector2(176,-165),new Vector2(189,-142),new Vector2(199,-118),new Vector2(209,-93),new Vector2(216,-68),new Vector2(222,-41),new Vector2(225,-14),new Vector2(229,12),new Vector2(231,39),new Vector2(228,66),new Vector2(223,92),new Vector2(217,119),new Vector2(211,145),new Vector2(203,170),new Vector2(189,191),new Vector2(172,209),new Vector2(154,225),new Vector2(134,236),new Vector2(113,245),new Vector2(91,248),new Vector2(69,248),new Vector2(46,249),new Vector2(24,249),new Vector2(2,245),new Vector2(-18,239),new Vector2(-40,231),new Vector2(-61,222),new Vector2(-82,213),new Vector2(-102,203),new Vector2(-122,191),new Vector2(-142,177),new Vector2(-158,159),new Vector2(-173,139),new Vector2(-183,115),new Vector2(-194,92),new Vector2(-205,68),new Vector2(-215,44),new Vector2(-220,20),new Vector2(-222,-5)});
      
        // add all templates
		Templates = new ArrayList(new ArrayList[] {circle, circle2});
        TemplateNames = new ArrayList(new string[] {"circle", "circle2"});
    }
}