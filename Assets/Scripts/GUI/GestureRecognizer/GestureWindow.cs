﻿using UnityEngine;
using System.Collections;

public class GestureWindow : MonoBehaviour {
	public LineRenderer GestureLineRenderer;

	public float StartWidth = .5f;
	public float EndWidth = .2f;

	
	private int _LineCount = 0;
	// Use this for initialization
	void Start () {
		_LineCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnMouseDown(){
		Gesture.Instance.RecordPattern = true;
	}

	void OnMouseOver(){
		if(Gesture.Instance.RecordPattern && Input.GetMouseButton(0))
			UpdateLine();
	}

	void UpdateLine(){
		GestureLineRenderer.SetWidth(StartWidth,EndWidth);
		GestureLineRenderer.SetVertexCount(Gesture.Instance.pointArr.Count);

		for(int i = _LineCount; i < Gesture.Instance.pointArr.Count; i++){
			GestureLineRenderer.SetPosition(i,GameManager.Instance.CurrentMousePosition);
		}

		_LineCount = Gesture.Instance.pointArr.Count;
	}
}
