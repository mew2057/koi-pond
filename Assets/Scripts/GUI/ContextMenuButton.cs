﻿using UnityEngine;
using System.Collections;

public class ContextMenuButton : MonoBehaviour {
	public Material OptionDefault;
	public Material OptionSelected;
	public CharacterTool EquippedTool;
	public Transform TransformToScale;

	private float _ScaleFactor = 1;
	private Renderer _MaterialRenderer;
	private bool _Hovered = false;

	// Use this for initialization
	void Start () {
		_MaterialRenderer = GetComponent<Renderer>();
		_MaterialRenderer.material = OptionDefault;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void SetHover(){
		// XXX magic numbers
		//_ScaleFactor = _Hovered ? 1.5f : 1;

		_MaterialRenderer.material = _Hovered ? OptionSelected : OptionDefault;
		TransformToScale.localScale = _Hovered ? 1.5f * TransformToScale.localScale: new Vector3(1f,1f,1f);
	}

	void OnMouseOver(){
		if(!_Hovered){
			_Hovered = true;
			SetHover();
		}

		if(Input.GetMouseButtonUp(0)){
			ContextManager.Instance.MenuSelect(EquippedTool);
			OnMouseExit();
		}
	}


	void OnMouseExit(){
		_Hovered = false;
		SetHover();
		_ScaleFactor = 1;
	}
}
