﻿using UnityEngine;
using System.Collections;

public class GestureButtonManager : MonoBehaviour {
	public GestureButton save;
	public GestureButton cancel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(save.Active || cancel.Active){
			Gesture.Instance.DoMyWindow(save.Active? GestureOptions.Save : GestureOptions.Cancel);
			save.Active = false;
			cancel.Active = false;
		}
	}
}
