﻿using UnityEngine;
using System.Collections;

public class GestureButton : MonoBehaviour {
	public bool Active=false;

	void OnMouseDown(){
		renderer.material.color=Color.red;
	}

	void OnMouseUp(){
		Active = true;
		renderer.material.color = Color.white;
	}

}
