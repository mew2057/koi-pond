﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {
	public static MenuController Instance;
	public Transform StartCameraTransform;
	public Transform EndCameraTransform;
	public GameObject HideableMenu;

	public float LerpTime = 5f;
	public float ClickDelayFactor = 10;
	private Transform _TransformGoal;
	private bool _AtStart = true;
	private bool _PanActive=false;
	private float _TimeTotal = 0f;

	// Use this for initialization
	void Start () {
		if(Instance == null)
			Instance = this;

		//StartCameraTransform = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
		if(_PanActive){
			_TimeTotal+=Time.deltaTime;

			Camera.main.transform.position = 
				Vector3.Lerp(Camera.main.transform.position, 
				             _TransformGoal.position,
				             _TimeTotal/LerpTime);

			Camera.main.transform.rotation = 
				Quaternion.Slerp(Camera.main.transform.rotation, 
				                 _TransformGoal.rotation,
				                 _TimeTotal/LerpTime);

			if(_AtStart && _TimeTotal > LerpTime/ClickDelayFactor)
				HideableMenu.SetActive(true);
			else if(_TimeTotal > LerpTime/ClickDelayFactor)
				HideableMenu.SetActive(false);

			
			if(_TimeTotal >= LerpTime || (Input.GetMouseButtonDown(0) && _TimeTotal > LerpTime/ClickDelayFactor)){
				Camera.main.transform.rotation = _TransformGoal.rotation;
				Camera.main.transform.position = _TransformGoal.position;
				_PanActive = false;
				_TimeTotal = 0;

			}
		}
	}

	public void TogglePosition(){
		_TransformGoal = _AtStart ?  EndCameraTransform : StartCameraTransform;

		Vector3.Distance(Camera.main.transform.position,_TransformGoal.position);
		_TimeTotal = 0;
		_AtStart = !_AtStart;
		_PanActive = true;
	}
}
