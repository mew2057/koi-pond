﻿using UnityEngine;
using System.Collections;

public class Slider : MonoBehaviour {
	public GameObject Slidable;
	private float _MaxX;
	private float _MinX;


	void Start(){
		Bounds _Bounds = GetComponent<MeshFilter>().mesh.bounds; 
		_MaxX = Vector3.Scale(_Bounds.max, transform.localScale).x + transform.position.x;
		_MinX = Vector3.Scale(_Bounds.min, transform.localScale).x +  transform.position.x;

	}

	void MoveSlider(){
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if(collider.Raycast(ray, out hit,100.0f) &&
		   hit.point.x > _MinX && 
		   hit.point.x < _MaxX){
			Slidable.transform.position = new Vector3(
				hit.point.x,
				Slidable.transform.position.y,
				Slidable.transform.position.z);
		}
	}

	void OnMouseDrag(){
		MoveSlider();
	}

	void OnMouseDown(){
		MoveSlider();
	}


}
