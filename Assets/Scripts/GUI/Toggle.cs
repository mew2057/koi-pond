﻿using UnityEngine;
using System.Collections;

public class Toggle : MonoBehaviour {
	public bool Toggled = false;
	public int Set = 0;
	public int Unset = 1;

	public Texture[] Options;
	public bool ToggledFlag = false;
	private 
	// Use this for initialization
	void Start () {
		renderer.material.mainTexture = Options[Toggled?Set:Unset];
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown(){
		Toggled = !Toggled;
		ToggledFlag = true;
		SetToggle();
	}

	public void SetToggle(){
		renderer.material.mainTexture = Toggled?Options[Set]:Options[Unset];
	}
}
