﻿using UnityEngine;
using System.Collections;

public class BirdcallManager : MonoBehaviour {
	public float BaseTime = 2f;
	public float MaxTime = 6f;

	private AudioSource[] _Birdcalls;
	private float[] _Timers;

	// Use this for initialization
	void Start () {
		_Birdcalls = GetComponentsInChildren<AudioSource>();
		_Timers = new float[_Birdcalls.Length];

		for(int i=0; i < _Birdcalls.Length; i++){
			_Timers[i] = GenerateBirdcall();
		}
	}
	
	// Update is called once per frame
	void Update () {
		for(int i=0; i < _Birdcalls.Length; i++){
			_Timers[i] -= Time.deltaTime;

			if(_Birdcalls[i] && _Timers[i] <= 0){
				_Birdcalls[i].Play();
				_Timers[i] = GenerateBirdcall();
			}
		}
	}

	float GenerateBirdcall(){
		return Random.Range(BaseTime,MaxTime);
	}
}
