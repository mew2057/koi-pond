﻿using UnityEngine;
using System.Collections;

public class AI_helper : MonoBehaviour {
	public GameObject[] fishArray;
	public int numFishies;
	
	//values used by all flockers that are calculated by controller on update
	public Vector3 flockDirection;
	public Vector3 centroid;
	
	// weight parameters are set in editor and used by all flockers 
	// if they are initialized here, the editor will override settings	 
	// weights used to arbitrate btweeen concurrent steering forces 
	public float alignmentWt = 0f;
	public float separationWt = 0f;
	public float cohesionWt = 0f;
	public float tetherWt = 0f;
	public float avoidWt = 0f;
	public float totalWeight = 0;

	// Use this for initialization
	void Start () {
		fishArray = GameObject.FindGameObjectsWithTag("Fish");
		numFishies = fishArray.Length;
	}
	
	// Update is called once per frame
	void Update () {
		calcCentroid( );//find average position of each flocker 
		calcFlockDirection( );//find average "forward" for each flocker
		totalWeight = alignmentWt + separationWt + cohesionWt + tetherWt + avoidWt;
	}
	
	private void calcCentroid ()
	{
		// calculate the current centroid of the flock
		// use transform.position
		centroid = new Vector3();
		//calc the centroid of the flock and calc the avg heading of the flock
		//flock center: add up all the pos vectors then divide by the num of boids
		//avg heading: add up all the fwd vectors then divide by the num of boids
		
		for (int i = 0; i < numFishies; i++)
		{
			centroid = centroid+(fishArray[i].transform.position);
		}
		centroid = centroid/(numFishies);
	}
	
	private void calcFlockDirection ()
	{
		// calculate the average heading of the flock
		// use transform.
		flockDirection = new Vector3();
			
		for (int i = 0; i < numFishies; i++)
		{
			flockDirection = flockDirection + fishArray[i].transform.forward;
		}
		flockDirection = flockDirection / numFishies;
	}
}
