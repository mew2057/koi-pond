﻿using UnityEngine;
using System.Collections;

public class CricketManager : MonoBehaviour {
	public static CricketManager Instance;

	private string[] Sayings = {
		"Don't Clean Pond! Clean Soul!",
		"The water reflects your soul.",
		"Cleanliness is next to Godliness.",
		"Whenever possible, keep it simple.",
		"Unleash your life force.",
		"Try? No! Do or do not, there is no try.",
		"Good things take time.",
		"Make a wise choice everyday.",
		"Failure is the mother of all success.",
		"Aim high, time flies.",
		"You create enthusiasm around you.",
		"Let your imagination wander.",
		"Hope brings about a better future.",
		"Patience is a key to joy.",
		"Time is an illusion. " +
		"Lunchtime doubly so.",
		"42",
		"You need to take more risks in your life."
	};

	public GameObject[] SpawnLocations;

	public GameObject cricketPrefab;

	public static bool CricketLive=false;

	public FortuneCookie Cookie;

	private bool Spawning = false;
	float timer = 0;
	float CurrentTime = 0;
	

	// Use this for initialization
	void Start () {
		if(Instance == null)
			Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		if(CricketManager.CricketLive == false && !Spawning){
			timer = Random.Range(10,30);
			CurrentTime = 0;
			Spawning = true;
		}
		else if(CricketManager.CricketLive == false){
			CurrentTime+=Time.deltaTime;

			if(CurrentTime>=timer)
			{
				int spawnIndex = Random.Range(0,SpawnLocations.Length)%SpawnLocations.Length;
				Instantiate(cricketPrefab,
				            SpawnLocations[spawnIndex].transform.position,
				            SpawnLocations[spawnIndex].transform.rotation);

				Spawning = false;
			}
		}			
	}

	public void UpdateCookie(){
		int spawnIndex = Random.Range(0,Sayings.Length)%Sayings.Length;
		Cookie.ShowCookie(Sayings[spawnIndex]);
	}
}
