﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {
	
	public GameObject food;

	public Collider SpawnPlane; 

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// This draws a raycast to a hitbox for precise dropping.
		if(Input.GetMouseButtonDown(0) && GameManager.Instance.EquippedTool.Equals(CharacterTool.Food)){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			RaycastHit hit = new RaycastHit();

			if(SpawnPlane.Raycast(ray, out hit, 50))
			{
				Instantiate(food,hit.point, Quaternion.Euler(new Vector3(0,0,0)));
			}			          
		}
	}
}
