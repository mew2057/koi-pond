﻿using UnityEngine;
using System.Collections;

//directive to enforce that our parent Game Object has a Character Controller
[RequireComponent(typeof(CharacterController))]

public class Vehicle : MonoBehaviour 
{
	//The Character Controller on my parent GameObject
	CharacterController characterController;
	
	/// <summary>
	/// The move direction for a frame.
	/// </summary>
	private Vector3 moveDirection;
	
	/// <summary>
	/// The linear gravity factor. Made available in the Editor.
	/// </summary>
	public float gravity = 100.0f;
	
	
	/// <summary>
	/// The initial orientation.
	/// </summary>
	private Quaternion initialOrientation;
	
	/// <summary>
	/// The cummulative rotation for this frame.
	/// </summary>
	private float cummulativeRotation;
	
	/// <summary>
	/// The rotation factor, this controls the speed we rotate at.
	/// </summary>
	public float rotationFactor = 300.0f;

	//
	private Vector3 hitNormal;
	public float lookAheadDist = 12.0f; 
	public float halfHeight = 2.8f;
	private Vector3 lookAtPt;
	private Vector3 rayOrigin;  
	private RaycastHit rayInfo;
	private int layerMask = 1 << 8;
	public GameObject scout;
	
		//movement variables
	private float speed = 0.0f;
	private float maxSpeed = 150.0f;
	
	//steering variables
	private Vector3 steeringForce;
	private float maxForce = 100.0f;
	private float friction = 0.97f;

	
	// Use this for initialization
	void Start () 
	{
		//Here I grab the reference to the Character Controller through this 
		//generic method on my parent Game Object. The class I put in the <> 
		//is the Component type I am attempting to get a reference to.
		characterController = gameObject.GetComponent<CharacterController>();
		
		//set our moveDirection initially to the vector (0, 0, 0)
		moveDirection = Vector3.zero;
		
		//snag the initial orientation from our transform
		initialOrientation = transform.rotation;
		
		//default the cummulativeRotation to zero.
		cummulativeRotation = 0.0f;		
		
	}

//-----------------------------------hitNormal mod------------------------------------		

	void OnControllerColliderHit(ControllerColliderHit hit) {
		
		hitNormal = hit.normal;
	}
	

	
	// Update is called once per frame
	void Update ()
	{
		//Rotate before translation.
		//Get Input from the Mouse and use time along with the scaling factor 
		//to rotate at a smooth rate.
		cummulativeRotation += Input.GetAxis ("Mouse X") * Time.deltaTime * rotationFactor;
		
		//Create a Quaternion from the Euler Angles we specify. 
		//We only want to rotate about the Y axis to turn our Avatar. 
		Quaternion currentRotation = Quaternion.Euler (0.0f, cummulativeRotation, 0.0f);
		
		//Now we set the rotation of this digger's Game Object based on initial orientation 
		//and the currently applied rotation from the original orientation. 
		transform.rotation = initialOrientation * currentRotation;


//-----------------------------------acceleration mod------------------------------------		
		CalcSteering ();
		ClampSteering ();
		moveDirection = transform.forward;
		speed *= friction;
		moveDirection *= speed;
		// now movedirection equals velocity
		
		// add acceleration
		moveDirection += steeringForce * Time.deltaTime;
		
		//update speed
		speed = moveDirection.magnitude;
		if (speed > maxSpeed) 
		{
			speed = maxSpeed;
			moveDirection = moveDirection.normalized * maxSpeed;
		}

		//orient transform
		if (moveDirection != Vector3.zero)
		{
			transform.forward = moveDirection;
		}
		
		//Now move 'down' based on gravity.
		moveDirection.y -= gravity;
		
		//Apply net movement to character controller which
		//keeps us from penetrating colliders.
		characterController.Move (moveDirection * Time.deltaTime);
		alignWithTerrain ();
	}
	
	//-----------------------------------align with terrain function------------------------------------		

	void alignWithTerrain ()
	{
		
		rayOrigin = transform.position + transform.forward * lookAheadDist;
		rayOrigin.y += 1000;
		if (Physics.Raycast (rayOrigin, Vector3.down, out rayInfo, Mathf.Infinity, layerMask)) {
			lookAtPt = rayInfo.point;
			lookAtPt.y += halfHeight;
			scout.transform.position = lookAtPt;
		}
		transform.LookAt (lookAtPt, hitNormal);
	}
	
	private Vector3 UserInput( )
	{
		//Move 'forward' based on player input
		Vector3 sf = Vector3.zero;//sf is steering force
		sf.z = Input.GetAxis("Vertical"); //forward is positive z 
		//Take the moveDirection from the vehicle's local space to world space 
		//using the transform of the Game Object this script is attached to.
		sf = transform.TransformDirection(sf);
		sf *= maxSpeed;
		return sf;
	}
	
	
	private void CalcSteering( )
	{
		steeringForce = Vector3.zero;
		steeringForce += UserInput( );
	}
	
	
	private void ClampSteering( )
	{
		if (steeringForce.magnitude > maxForce)
		{
			steeringForce.Normalize( );
			steeringForce *= maxForce;
		}		
	}
}
