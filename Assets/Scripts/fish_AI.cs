﻿using UnityEngine;
using System.Collections;

public class fish_AI : MonoBehaviour {
	public Vector3 movement;
	public Vector3 tempVector3;
	public Vector3 steeringForce;
	public Vector3 centerScreenDirection;
	
	public Quaternion desiredRot;
	
	//movement variables
	private float speed = 3f;
	public float maxForce = 35;
	public float maxSpeed = 30;
	
	//debug prints
	public Vector3 fwd_DEV_DISPLAY;
	public Vector3 RB_velocity_DEV_DISPLAY;
	public Vector3 RB_angularVelocity_DEV_DISPLAY;
	public Quaternion RB_rotation_DEV_DISPLAY;
	
	//accessors and mutators
	public float Speed {
		get { return speed; }
		set { speed = Mathf.Clamp (value, 0, maxSpeed); }
	}
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//rigidbody.AddRelativeForce (Vector3.forward);
		fwd_DEV_DISPLAY = Vector3.forward;
		RB_velocity_DEV_DISPLAY = rigidbody.velocity;
		RB_angularVelocity_DEV_DISPLAY = rigidbody.angularVelocity;
		RB_rotation_DEV_DISPLAY = rigidbody.rotation;
		
	}
	
	void FixedUpdate(){
		//Move Forward
		//TetherToCenter();
		steeringForce = Vector3.zero;
		steeringForce += transform.parent.gameObject.GetComponent<AI_helper>().tetherWt / transform.parent.gameObject.GetComponent<AI_helper>().totalWeight * TetherToCenter();
		steeringForce += transform.parent.gameObject.GetComponent<AI_helper>().cohesionWt / transform.parent.gameObject.GetComponent<AI_helper>().totalWeight * Cohesion();
		steeringForce += transform.parent.gameObject.GetComponent<AI_helper>().alignmentWt / transform.parent.gameObject.GetComponent<AI_helper>().totalWeight * Align();
		desiredRot = transform.rotation * Quaternion.FromToRotation(transform.forward, steeringForce);
		transform.rotation = Quaternion.Slerp(transform.rotation, desiredRot, .1f);
		transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
		
		rigidbody.AddRelativeForce(Vector3.forward);
	}
	
	public float Radius {
		get {
			Mesh mesh = GetComponent<MeshFilter> ().mesh;
			//Debug.Log (mesh.bounds.size.x * transform.localScale.x);
			float x = mesh.bounds.size.x * transform.localScale.x;
			float z = mesh.bounds.size.z * transform.localScale.z;
			return Mathf.Sqrt (x * x + z * z);
		}
	}
	
#region basic steering
	public Vector3 flee (Vector3 pos)
	{
		Vector3 dv = transform.position - pos;//opposite direction from seek 
		dv.y = 0;
		dv = dv.normalized * maxSpeed;
		return dv;
	}
	
	public Vector3 seek (Vector3 pos)
	{
		// find dv, the desired velocity
		Vector3 dv = pos - transform.position;
		dv.y = 0; //only steer in the x/z plane
		dv = dv.normalized;
		return dv;
	}
	
	public Vector3 alignTo (Vector3 direction)
	{
		// useful for aligning with flock direction
		Vector3 dv = direction.normalized;
		dv.y = 0; //stay in x/z plane
		return dv * maxSpeed;
		
	}
#endregion
	
	public Vector3 TetherToCenter(){
		//Move Toward Center
		
		return seek(GameObject.FindGameObjectWithTag("Center_of_World").transform.position);
		
	}
	
	public Vector3 Cohesion(){
		
		return seek(transform.parent.gameObject.GetComponent<AI_helper>().centroid);
	}
	
	public Vector3 Align(){
		return alignTo(transform.parent.gameObject.GetComponent<AI_helper>().flockDirection);
	}
}
