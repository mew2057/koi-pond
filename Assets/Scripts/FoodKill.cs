﻿using UnityEngine;
using System.Collections;

public class FoodKill : MonoBehaviour {
	public int deathClock = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(deathClock > 666)
			Destroy(gameObject);
	}
	
	public void IncreaseDeathTimer(){
		deathClock++;
	}
	
	void OnTriggerEnter (Collider col)
    {
		if(col.CompareTag("Fish"))
        	Destroy(gameObject);
    }
}
