﻿using UnityEngine;
using System.Collections;

public class Skimmer : MonoBehaviour {
	private int _SpawnID;
	// Use this for initialization
	void Start () {
		_SpawnID = ALSpawner.Instance.CurrentSpawnID-1;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseOver() {
		RemoveSkimable();
	}

	void OnMouseDown(){
		RemoveSkimable();
	}

	void RemoveSkimable(){
		if(GameManager.Instance.EquippedTool == CharacterTool.Skimmer){
			Destroy(gameObject);
			ALSpawner.Instance.ObjDestroyed(_SpawnID);
		}
	}
}
