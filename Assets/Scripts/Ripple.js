﻿
private var buffer1 : int[];
private var buffer2 : int[];
private var  vertexIndices : int[];

private var mesh : Mesh ;

private var vertices :  Vector3[] ;
//private Vector3[] normals ;

public var dampner :  float  = 0.999;
public var  maxWaveHeight :  float = 2.0;

public var splashForce :  int= 1000;

//public int slowdown = 20;
//private int slowdownCount = 0;
private var swapMe : boolean= true;
private var mainCam : Camera;
public var  cols : int = 128;
public var rows : int = 128;
public var waitsecond : float = 0.3;
public var inc :  float = 0.0;
public var droptimes  : int= 0;
private var col : int;
private var row : int;

	// Use this for initialization
function Start () {
		var mf : MeshFilter =GetComponent(MeshFilter);
		mesh = mf.mesh;
	    vertices = mesh.vertices;
		buffer1 = new int[vertices.Length];
		buffer2 = new int[vertices.Length];
	mainCam = GameObject.Find("Main Camera").camera;
	var bounds : Bounds= mesh.bounds;
    
    var  xStep : float = (bounds.max.x - bounds.min.x)/cols;
    var  zStep : float = (bounds.max.z - bounds.min.z)/rows;

	vertexIndices = new int[vertices.Length];	
    var i  : int= 0;
	for (i = 0; i < vertices.Length; i++)
	{
		vertexIndices[i] = -1;
		buffer1[i] = 0;
		buffer2[i] = 0;
	}
    
    // this will produce a list of indices that are sorted the way I need them to 
    // be for the algo to work right
	for (i = 0; i < vertices.Length; i++) {
		var column : float = ((vertices[i].x - bounds.min.x)/xStep);// + 0.5;
		var row : float = ((vertices[i].z - bounds.min.z)/zStep);// + 0.5;
		var position  : int= (row * (cols + 1)) + column + 0.5;
		if (vertexIndices[position] >= 0);
		vertexIndices[position] = i;	
	}

//	splashAtPoint(cols/2,rows/2);
}


function splashAtPoint( x : int,  y : int) {
	var position  : int = ((y * (cols + 1)) + x);
	try{
	buffer1[position] = splashForce;
    buffer1[position - 1] = splashForce;
	buffer1[position + 1] = splashForce;
	buffer1[position + (cols + 1)] = splashForce;
	buffer1[position + (cols + 1) + 1] = splashForce;
	buffer1[position + (cols + 1) - 1] = splashForce;
	buffer1[position - (cols + 1)] = splashForce;
	buffer1[position - (cols + 1) + 1] = splashForce;
	buffer1[position - (cols + 1) - 1] = splashForce;
	}
	catch(e){}
}

// Update is called once per frame
function Update () {
	
	checkInput();
	
	var currentBuffer  : int[];
	if (swapMe) {
	// process the ripples for this frame
	    processRipples(buffer1,buffer2);
	    currentBuffer = buffer2;
	} else {
	    processRipples(buffer2,buffer1);		
	    currentBuffer = buffer1;
	}
	swapMe = !swapMe;
	// apply the ripples to our buffer
    var theseVertices : Vector3[] = new Vector3[vertices.Length];
 	var  vertIndex : int;
 	var i : int = 0;
    for (i = 0; i < currentBuffer.Length; i++)
    {
    	vertIndex = vertexIndices[i];
        theseVertices[vertIndex] = vertices[vertIndex];
        theseVertices[vertIndex].y +=  (currentBuffer[i] * 1.0/splashForce) * maxWaveHeight;
    }
    mesh.vertices = theseVertices;


    // swap buffers		
}

function checkInput() {	
 if (Input.GetMouseButton (0)) {
	var  hit : RaycastHit;
	if (Physics.Raycast (Camera.main.ScreenPointToRay(Input.mousePosition), hit, 1000)) {
    	var  bounds : Bounds= mesh.bounds;
    	var xStep : float = (bounds.max.x - bounds.min.x)/cols;
        var zStep : float = (bounds.max.z - bounds.min.z)/rows;
    	var  xCoord : float = (bounds.max.x - bounds.min.x) - ((bounds.max.x - bounds.min.x) * hit.textureCoord.x);
    	var  zCoord : float= (bounds.max.z - bounds.min.z) - ((bounds.max.z - bounds.min.z) * hit.textureCoord.y);
    	col =(xCoord/xStep);// + 0.5;
		row =(zCoord/zStep);// + 0.5;
		
		droptimes = 3;
	}
 }
 
 if (droptimes > 0)
 {
 	waitsecond-=Time.deltaTime;
	if (waitsecond <= 0)
	{
		splashAtPoint(col,row);
		waitsecond = 0.3 - inc;
		//maxWaveHeight -= 0.1f;
		droptimes--;
		
		inc+= 0.1;
	}
		
 }
 else
 {
 	//maxWaveHeight = 1.0f;
 //	inc = 0.0f;
 	
 }

}
 
function processRipples(source : int[], dest  : int[]) {
	var x : int = 0;
	var y : int = 0;
	var position : int= 0;
	for ( y = 1; y < rows - 1; y ++) {
		for ( x = 1; x < cols ; x ++) {
			position = (y * (cols + 1)) + x;
			dest [position] = (((source[position - 1] + 
								 source[position + 1] + 
								 source[position - (cols + 1)] + 
								 source[position + (cols + 1)]) >> 1) - dest[position]);  
		   dest[position] =(dest[position] * dampner);
		}			
	}	
}

