﻿using UnityEngine;
using System.Collections;

public class Flowers : MonoBehaviour {
	
	//public Color colorStart = Color.red;
	//public Color colorEnd = Color.grey;
	//public float duration = 1.0F;
	float timer;
	bool flag = true;
	bool wilt = false;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(flag == true)
		{
			flag=false;
			timer = Time.time + 5 + Random.Range(20, 28);
		}
		if(Time.time > timer)
		{
			wilt = true;
			renderer.material.shader = Shader.Find("Specular");
			renderer.material.SetColor("_SpecColor", Color.yellow);
		
		}
		
	}
	
	void OnMouseOver(){
		
		if(GameManager.Instance.EquippedTool == CharacterTool.WateringCan)
		{
			wilt = false;
			flag = true;
			renderer.material.shader = Shader.Find("Transparent/Cutout/Soft Edge Unlit");
		}
	}
}
