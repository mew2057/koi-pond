﻿using UnityEngine;
using System.Collections;

public class FortuneCookie : MonoBehaviour {
	public TextMesh FortuneText;
	// Use this for initialization
	void Start () {
		HideCookie();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnMouseDown(){
		HideCookie();
	}
	public void HideCookie(){
		renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, 0);
		FortuneText.text = "";
		collider.enabled = false;
	}

	public void ShowCookie(string saying){
		renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, 1);
		FortuneText.text = saying;
		collider.enabled = true;
	}
}
