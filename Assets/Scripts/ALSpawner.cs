﻿using UnityEngine;
using System.Collections;

public enum SkimType{
	Leaves, Algae
}

public class ALSpawner : MonoBehaviour {
	public static ALSpawner Instance;

	public int CurrentSpawnID = 0;
	public Hashtable  SpawnedItems = new Hashtable ();

	public SkimType SkimableToCreate = SkimType.Leaves;


	public GameObject LeafPrefab;
	public GameObject AlgaePrefab;
	
	public float XStep = 5;
	public float ZStep = 5;
	public float stepModX = 1;
	public float stepModZ = 1;
	
	public int MaxSkimSpawn =10;
	public int MinSkimSpawn = 4;

	public int MaxTimeSpawn =7;
	public int MinTimeSpawn =2;

	private float _TimeElapsed = 0f;
	private float _TimeToSpawnNext = -1f;
	private Bounds _SpawningBounds;

	// Use this for initialization
	void Awake () {
		if(Instance == null){
		   Instance = this;
			_SpawningBounds = GetComponent<Renderer>().bounds;
		}
		SpawnChain();
		
	}
	
	// Update is called once per frame
	void Update () {
		if(_TimeToSpawnNext >0){
			_TimeElapsed+=Time.deltaTime;

			if(_TimeElapsed >= _TimeToSpawnNext){
				SpawnChain();
				_TimeToSpawnNext = -1;
			}
		}
	}

	void SpawnChain(){

		GameObject prefabToSpawn = null;
		int spawn = Random.Range(0,2);
		switch(spawn){
		case 0:
			SkimableToCreate = SkimType.Leaves;
			prefabToSpawn = LeafPrefab;
			break;
		case 1:
			SkimableToCreate = SkimType.Algae;
			prefabToSpawn = AlgaePrefab;
			break;
		}

		float xStep = Random.Range(_SpawningBounds.min.x,_SpawningBounds.max.x);
		float zStep = Random.Range(_SpawningBounds.min.z,_SpawningBounds.max.z);
		
		float randomChainLength = Random.Range(MinSkimSpawn,MaxSkimSpawn);

		// Add the dictionary entry.
		SpawnedItems.Add(CurrentSpawnID,randomChainLength);

		for(int items = 0; items < randomChainLength; items++){
			if(_SpawningBounds.min.x < xStep &&  xStep< _SpawningBounds.max.x && _SpawningBounds.min.z < zStep && zStep < _SpawningBounds.max.z)
			{
				Instantiate(prefabToSpawn,
			            new Vector3(xStep,transform.position.y,zStep), 
			            Quaternion.Euler(new Vector3(90,0,0)));
				items++;
			}
			items--;
			
			stepModX = Random.Range(0.8F, 1.2F);
			stepModZ = Random.Range(0.8F, 1.2F);
			xStep = xStep * stepModX;
			zStep = zStep * stepModZ;
			
			
				switch(Random.Range(0,8)){
				case 0:
					xStep += XStep;
					break;
				case 1:
					zStep += ZStep;
					break;
				case 2:
					xStep += XStep;
					zStep += ZStep;
					break;
				case 3:
					xStep -= XStep;
					break;
				case 4:
					zStep -= ZStep;
					break;
				case 5:
					xStep -= XStep;
					zStep -= ZStep;
					break;
				case 6:
					xStep += XStep;
					zStep -= ZStep;
					break;
				case 7:
					xStep -= XStep;
					zStep += ZStep;
					break;
			}
		}

		CurrentSpawnID++;


	}

	public void ObjDestroyed(int id){
		int itemVal = int.Parse(SpawnedItems[id].ToString()) - 1;

		if(itemVal <=0){
			SpawnedItems.Remove(id);
			SpawnNew();
		}
		else{
			SpawnedItems[id] = itemVal;
		}
	}

	public void SpawnNew(){
		_TimeElapsed = 0f;
		_TimeToSpawnNext = Random.Range(MinTimeSpawn,MaxTimeSpawn);
	}
}
