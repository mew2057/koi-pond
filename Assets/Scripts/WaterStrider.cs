﻿using UnityEngine;
using System.Collections;
using System;
public class WaterStrider : MonoBehaviour {

	float movementSpeed=0.0f;
	int dir;
	float startTime = 0f;
	float sT=0f;
	bool canMove = true;
	
	void Update () 
	{
		if(canMove)
		{
			System.Random t = new System.Random();
			
			
					
			if(movementSpeed==0.0)
			{
				dir = t.Next(1, 9);
				sT=Time.time;
				movementSpeed = 3.0f;
				canMove = false;
				startTime = Time.time;
			}
			
			
			if(dir==1 || dir==9)
			{	
				transform.position +=Vector3.forward * movementSpeed * Time.deltaTime;
				dir = 9;
				if(Time.time>sT+1)
				{
					movementSpeed--;
					sT=Time.time;
				}
				if(movementSpeed==0)
					dir=1;
			}
			else if(dir==2 || dir==10)
			{
				transform.position +=Vector3.forward * movementSpeed * Time.deltaTime;
				transform.position +=Vector3.right * movementSpeed * Time.deltaTime;
				dir=10;
				if(Time.time>sT+1)
				{
					movementSpeed--;
					sT=Time.time;
				}
				if(movementSpeed==0)
					dir=2;
			}
			else if(dir==3 || dir==11)
			{
				transform.position +=Vector3.right * movementSpeed * Time.deltaTime;
				dir=11;
				if(Time.time>sT+1)
				{
					movementSpeed--;
					sT=Time.time;
				}
				if(movementSpeed==0)
					dir=3;
			}
			else if(dir==4 || dir==12)
			{
				transform.position -=Vector3.forward * movementSpeed * Time.deltaTime;
				transform.position +=Vector3.right * movementSpeed * Time.deltaTime;
				dir=12;
				if(Time.time>sT+1)
				{
					movementSpeed--;
					sT=Time.time;
				}
				if(movementSpeed==0)
					dir=4;
			}
			else if(dir==5 || dir==13)
			{
				transform.position -=Vector3.forward * movementSpeed * Time.deltaTime;
				dir=13;
				if(Time.time>sT+1)
				{
					movementSpeed--;
					sT=Time.time;
				}
				if(movementSpeed==0)
					dir=5;
			}
			else if(dir==6 || dir==14)
			{
				transform.position -=Vector3.forward * movementSpeed * Time.deltaTime;
				transform.position -=Vector3.right * movementSpeed * Time.deltaTime;
				dir=14;
				if(Time.time>sT+1)
				{
					movementSpeed--;
					sT=Time.time;
				}
				if(movementSpeed==0)
					dir=6;
			}
			else if(dir==7 || dir==15)
			{
				transform.position -=Vector3.right * movementSpeed * Time.deltaTime;
				dir=15;
				if(Time.time>sT+1)
				{
					movementSpeed--;
					sT=Time.time;
				}
				if(movementSpeed==0)
					dir=7;
			}
			else if(dir==8 || dir==16)
			{
				transform.position +=Vector3.forward * movementSpeed * Time.deltaTime;
				transform.position -=Vector3.right * movementSpeed * Time.deltaTime;
				dir=16;
				if(Time.time>sT+1)
				{
					movementSpeed--;
					sT=Time.time;
				}
				if(movementSpeed==0)
					dir=8;
			}
		}
		else
		{
			if(Time.time > startTime  + 2)
			{
				canMove = true;
				
				startTime = 0f;
			}
		}
		
	}
}
