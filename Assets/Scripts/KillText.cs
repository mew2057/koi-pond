﻿using UnityEngine;
using System.Collections;

public class KillText : MonoBehaviour 
{
	float timer;
	// Use this for initialization
	void Start () 
	{
		timer = Time.time + 5;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Time.time > timer)
		{
			Destroy(gameObject);
		}
	}
}
