using UnityEngine;
using System.Collections;

public enum CharacterTool{
	Skimmer, Food, WateringCan
}

public class GameManager : MonoBehaviour {

	public static GameManager Instance;

	public CharacterTool EquippedTool = CharacterTool.Skimmer;
	public int DistanceToCamera = 20;
	public Vector3 CurrentMousePosition;

	public string GestureName = "ContextMenu";
	public ContextManager ContextManager;

	public bool[] AudioFlags;

	public bool LongPressMenu = true;

	public AudioSource MusicSource;

	public float FadeTime = 1f;
	public bool FadeOut =false;

	void Awake(){
		if(GameManager.Instance == null){
			GameManager.Instance = this;
			DontDestroyOnLoad(this.gameObject);
			AudioFlags = new bool[]{true,true};
		}



		CurrentMousePosition =Camera.main.ScreenToWorldPoint(
			new Vector3(Input.mousePosition.x,
		            Input.mousePosition.y,
		            GameManager.Instance.DistanceToCamera));
	}

	void Update(){
		CurrentMousePosition = Camera.main.ScreenToWorldPoint(
			new Vector3(Input.mousePosition.x,
		            Input.mousePosition.y,
		            GameManager.Instance.DistanceToCamera));
	}

	public void SetEquippedTool(CharacterTool NewTool){
		EquippedTool = NewTool;
	}

	public void ContextGestureDetected(){
		if(ContextManager.Instance)
			ContextManager.Instance.ShowContextMenu();
	}

	public bool IsSFXEnabled(){
		return AudioFlags[1];
	}

	public bool	IsMusicPlaying(){
		return AudioFlags[0];
	}

	public void SetSFX(bool sfx){
		AudioFlags[1] = sfx;
	}
	
	public void	SetMusicPlaying(bool music){
		AudioFlags[0] = music;

		if(music)
			MusicSource.Play();
		else
			MusicSource.Pause();

	}

	public void LoadLevel(int level){
		CameraFade.StartAlphaFade( Color.black, false, 1f, 0f,  () => { Application.LoadLevel(level); } );
	}


}
